import React, {useEffect, useState} from 'react';
import Recipe from "./Recipe";
import './App.css';

const App = () => {

    const APP_ID = "e71cbc2a"
    const APP_KEY = "ef5f00675bada62b62440ca15abbd0db"

    const [recipes, setRecipes] = useState([])
    const [search, setSearch] = useState('')
    const [query, setQuery] = useState("chicken")

    // const [counter, setCounter] = useState(0)
    // first time page renders, useEffect() will run
    // after every time something re-renders on our page, it will also run
    // runs every time our page re-renders
    // if only want this to run once, can put [] as second argument in useEffect(..., [])
    // we only want to fetch data from API once when page load
    // if you want useEffect to run when a specific thing changes, add it to []
    // e.g. only want useEffect to run when counter changes --> useEffect(..., [counter])
    useEffect(() => {
        getRecipes()
    }, [query])

    // The async and await keywords enable asynchronous, promise-based behavior to be written in a cleaner
    // style, avoiding the need to explicitly configure promise chains
    const getRecipes = async () => {
        const response = await fetch(`https://api.edamam.com/search?q=${query}&app_id=${APP_ID}&app_key=${APP_KEY}`)
        const data  = await response.json()
        setRecipes(data['hits'])
        console.log(data['hits'])
    }

    const updateSearch = (e) => {
        setSearch(e.target.value)
    }

    const getSearch = (e) => {
        // prevent page refresh
        e.preventDefault()
        setQuery(search)
        setSearch("")
    }

    return(
        <div className={"App"}>
            <form className={"search-form"}>
                <input
                    type={"text"}
                    className={"search-bar"}
                    value={search}
                    onChange={updateSearch}
                />
                <button type={"submit"} className={"search-button"} onClick={getSearch}>
                    Search
                </button>
            </form>
            <div className={"recipes"}>
                {recipes.map(recipes => (
                    <Recipe
                        key={recipes['recipe'].label}
                        title={recipes['recipe'].label}
                        calories={recipes['recipe'].calories}
                        image={recipes['recipe'].image}
                        ingredients={recipes['recipe'].ingredients}
                    />
                ))}
            </div>
        </div>
    )
}
export default App;
